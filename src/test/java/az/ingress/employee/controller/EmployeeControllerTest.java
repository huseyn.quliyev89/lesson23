package az.ingress.employee.controller;

import az.ingress.employee.dto.EmployeeDto;
import az.ingress.employee.exception.ResourceNotFoundException;
import az.ingress.employee.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito. * ;

@WebMvcTest(EmployeeController.class)
//is a test runner provided by the Spring framework that helps integrate Spring features into your JUnit tests
@RunWith(SpringRunner.class)
class EmployeeControllerTest {
@Autowired
//virtual web browser that allows you to send HTTP requests to your Spring MVC controllers and receive the responses
private MockMvc mockMvc;

    @MockBean
    //use this annotation to create mock objects for the dependencies (fields) of the class you are testing
    private EmployeeService employeeService;
    private EmployeeDto employeeDto;

    private List<EmployeeDto> employeeDtoList;
    private List<EmployeeDto> employeeDtoEmptyList;

    private EmployeeDto updatedEmployeeDto;

    @BeforeEach
    //Use this to annotate a method that should be executed before each test method in the test class.
    public void setUp() {

        employeeDto = EmployeeDto.builder().firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(1000)).build();

        employeeDtoList = Arrays.asList(EmployeeDto.builder().firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(1000)).build(), EmployeeDto.builder().firstName("Huseyn2").lastName("Guliyev2").email("huseyn2.guliyev2@pashabank.az").phoneNumber("+994502296643").jobTitle("Core Banking System Developer").salary(new BigDecimal(2000)).build());

        employeeDtoEmptyList = Collections.emptyList();

        updatedEmployeeDto = EmployeeDto.builder().firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Senior Core Banking System Developer").salary(new BigDecimal(2000)).build();

    }

    @Test
    void givenValidEmployeeId_whenGetEmployeeById_thenReturnEmployee() throws Exception {
        // arrange
        when(employeeService.getEmployeeById(anyInt())).thenReturn(employeeDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/employee/1")).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("Huseyn")).andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("Guliyev")).andExpect(MockMvcResultMatchers.jsonPath("$.email").value("huseyn.guliyev@pashabank.az")).andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value("+994512296643")).andExpect(MockMvcResultMatchers.jsonPath("$.jobTitle").value("Core Banking System Developer")).andExpect(MockMvcResultMatchers.jsonPath("$.salary").value("1000"));
    }

    @Test
    void givenInvalidEmployeeId_whenGetEmployeeById_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(employeeService).getEmployeeById(anyInt());

        // act & assert
            mockMvc.perform(MockMvcRequestBuilders.get("/v1/employee/2")).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenEmployee_whenSaveEmployee_thenReturnEmployee() throws Exception {
        // arrange
        when(employeeService.saveEmployee(any())).thenReturn(employeeDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/employee").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(employeeDto))).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("Huseyn")).andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("Guliyev")).andExpect(MockMvcResultMatchers.jsonPath("$.email").value("huseyn.guliyev@pashabank.az")).andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value("+994512296643")).andExpect(MockMvcResultMatchers.jsonPath("$.jobTitle").value("Core Banking System Developer")).andExpect(MockMvcResultMatchers.jsonPath("$.salary").value("1000"));

        verify(employeeService, times(1)).saveEmployee(employeeDto);
    }

    @Test
    void givenValidEmployeeIdAndValidEmployee_whenUpdateEmployee_thenReturnEmployee() throws Exception {
        // arrange
        Integer employeeId = 1;

        when(employeeService.updateEmployee(anyInt(), any())).thenReturn(updatedEmployeeDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/employee/{employeeId}", employeeId).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(employeeDto))).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("Huseyn")).andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("Guliyev")).andExpect(MockMvcResultMatchers.jsonPath("$.email").value("huseyn.guliyev@pashabank.az")).andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value("+994512296643")).andExpect(MockMvcResultMatchers.jsonPath("$.jobTitle").value("Senior Core Banking System Developer")).andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(new BigDecimal(2000)));

        verify(employeeService, times(1)).updateEmployee(1, employeeDto);

    }

    @Test
    void givenInvalidEmployeeIdAndValidEmployee_whenUpdateEmployee_thenReturnException() throws Exception {
        // arrange
        Integer employeeId = 11;

        doThrow(ResourceNotFoundException.class).when(employeeService).updateEmployee(anyInt(), any());

        // act & assert
            mockMvc.perform(MockMvcRequestBuilders.put("/v1/employee/{employeeId}", employeeId).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(updatedEmployeeDto))).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenValidEmployeeId_whenDeleteEmployee_thenNothing() throws Exception {
        // arrange
        Integer employeeId = 1;
        doNothing().when(employeeService).deleteEmployee(anyInt());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/employee/{employeeId}", employeeId)).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().string(""));

    }

    @Test
    void givenInvalidEmployeeId_whenDeleteEmployee_thenReturnException() throws Exception {
        // arrange
        Integer employeeId = 11;

        doThrow(ResourceNotFoundException.class).when(employeeService).deleteEmployee(Mockito.anyInt());

        // act & assert
            mockMvc.perform(MockMvcRequestBuilders.delete("/v1/employee/{employeeId}", employeeId)).andExpect(MockMvcResultMatchers.status().isNotFound());

    }

    @Test
    void givenEmptyParameter_whenGetAllEmployees_thenReturnEmployeesList() throws Exception {
        // arrange
        when(employeeService.getAllEmployees()).thenReturn(employeeDtoList);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/employee")).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2)).andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value("Huseyn")).andExpect(MockMvcResultMatchers.jsonPath("$[1].firstName").value("Huseyn2"));
    }

    @Test
    void givenEmptyParameter_whenGetAllEmployees_thenReturnEmptyList() throws Exception {
        // arrange
        when(employeeService.getAllEmployees()).thenReturn(employeeDtoEmptyList);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/employee")).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));

    }

}
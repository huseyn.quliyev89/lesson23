package az.ingress.employee.service;

import az.ingress.employee.dto.EmployeeDto;
import az.ingress.employee.exception.ResourceNotFoundException;
import az.ingress.employee.mapper.EmployeeMapper;
import az.ingress.employee.model.Employee;
import az.ingress.employee.repository.EmployeeRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class EmployeeServiceTCLiveTest {

    @Autowired
    private EmployeeServiceImpl employeeService;

    @Autowired
    private EmployeeRepository employeeRepository;

    private Employee employee;

    private EmployeeDto employeeDto;

    private EmployeeDto updatedEmployeeDto;


    @Container
    static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:latest")
            .withDatabaseName("postgres")
            .withUsername("postgres")
            .withPassword("mysecretpassword");

    @DynamicPropertySource
    static void postgreSQLProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
    }

    @BeforeAll
    public static void beforeAll() {
        postgreSQLContainer.start();
    }


    @BeforeEach
    public void setUp() {

        employee = Employee.builder().id(1).firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal("1000")).build();

        employeeDto = EmployeeDto.builder().firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Core Banking System Developer").salary(new BigDecimal("1000")).build();

        updatedEmployeeDto = EmployeeDto.builder().firstName("Huseyn").lastName("Guliyev").email("huseyn.guliyev@pashabank.az").phoneNumber("+994512296643").jobTitle("Senior Core Banking System Developer").salary(new BigDecimal("2000")).build();

    }

    @Test
    void givenEmployee_whenSaveEmployee_thenReturnEmployee() {
        // arrange

        // act
        employeeService.saveEmployee(employeeDto);

        // assert
        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(1);
        assertThat(employeeList.get(0).getId()).isEqualTo(1);
        assertThat(employeeList.get(0).getFirstName()).isEqualTo("Huseyn");
        assertThat(employeeList.get(0).getLastName()).isEqualTo("Guliyev");
        assertThat(employeeList.get(0).getEmail()).isEqualTo("huseyn.guliyev@pashabank.az");
        assertThat(employeeList.get(0).getPhoneNumber()).isEqualTo("+994512296643");
        assertThat(employeeList.get(0).getJobTitle()).isEqualTo("Core Banking System Developer");
        assertThat(employeeList.get(0).getSalary()).isEqualTo(new BigDecimal("1000.00"));
    }

    @Test
    void givenEmptyParameter_whenGetAllEmployees_thenReturnEmployeesList() {
        // arrange
        employeeRepository.save(employee);

        // act
        List<EmployeeDto> resultEmployeeList = employeeService.getAllEmployees();

        // assert
        assertThat(resultEmployeeList).hasSize(1);
        assertThat(resultEmployeeList.get(0).getFirstName()).isEqualTo("Huseyn");
    }

    @Test
    void givenValidEmployeeId_whenGetEmployeeById_thenReturnEmployee() {
        // arrange

        Optional<Employee> byId = employeeRepository.findById(1);
        if (byId.isEmpty()) employeeRepository.save(employee);

        // act
        EmployeeDto returnEmployee = employeeService.getEmployeeById(1);

        // assert
        assertThat(returnEmployee).isNotNull();
        assertThat(returnEmployee.getFirstName()).isEqualTo("Huseyn");
        assertThat(returnEmployee.getLastName()).isEqualTo("Guliyev");
        assertThat(returnEmployee.getEmail()).isEqualTo("huseyn.guliyev@pashabank.az");
        assertThat(returnEmployee.getPhoneNumber()).isEqualTo("+994512296643");
        assertThat(returnEmployee.getJobTitle()).isEqualTo("Core Banking System Developer");
        assertThat(returnEmployee.getSalary()).isEqualTo(new BigDecimal("1000.00"));
    }

    @Test
    void givenInvalidEmployeeId_whenGetEmployeeById_thenReturnException() {
        // arrange

        // act & assert
        assertThatThrownBy(() -> employeeService.getEmployeeById(11)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> employeeService.getEmployeeById(11));
    }


    @Test
    void givenEmptyParameter_whenGetAllEmployees_thenReturnEmptyList() {
        // arrange
        employeeRepository.deleteAll();

        // act
        List<EmployeeDto> resultEmployeeList = employeeService.getAllEmployees();

        // assert
        assertThat(resultEmployeeList).isEmpty();
    }

    @Test
    void givenValidEmployeeIdAndValidEmployee_whenUpdateEmployee_thenReturnUpdatedEmployee() {
        // arrange

        Optional<Employee> byId = employeeRepository.findById(1);
        if (byId.isEmpty()) employeeRepository.save(employee);

        // act
        EmployeeDto savedEmployee = employeeService.updateEmployee(1, updatedEmployeeDto);

        // assert
        assertThat(savedEmployee.getJobTitle()).isEqualTo("Senior Core Banking System Developer");
        assertThat(savedEmployee.getSalary()).isEqualTo(new BigDecimal("2000"));
    }

    @Test
    void givenInvalidEmployeeIdAndValidEmployee_whenUpdateEmployee_thenReturnException() {
        // arrange

        // act & assert
        assertThatThrownBy(() -> employeeService.updateEmployee(11, updatedEmployeeDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> employeeService.updateEmployee(11, updatedEmployeeDto));
    }

    @Test
    void givenValidEmployeeId_whenDeleteEmployee_thenNothing() {
        // arrange
        Optional<Employee> byId = employeeRepository.findById(1);
        if (byId.isEmpty()) employeeRepository.save(employee);

        // act
        employeeService.deleteEmployee(employee.getId());

        // assert
        assertFalse(employeeRepository.existsById(1));

    }


    @Test
    void givenInvalidEmployeeId_whenDeleteEmployee_thenReturnException() {
        // arrange

        // act & assert
        assertThatThrownBy(() -> employeeService.deleteEmployee(11)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> employeeService.deleteEmployee(11));
    }


    @AfterAll
    public static void afterAll() {
        postgreSQLContainer.stop();
    }

}

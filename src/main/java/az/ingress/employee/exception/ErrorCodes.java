package az.ingress.employee.exception;

import lombok.Getter;

@Getter
public enum ErrorCodes {
    RESOURCE_NOT_FOUND("MS19-EXCEPTION-001"),
    BAD_REQUEST("MS19-EXCEPTION-002")
    ;

    final String code;

    ErrorCodes(String code) {
        this.code = code;
    }

}

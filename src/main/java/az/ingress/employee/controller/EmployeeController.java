package az.ingress.employee.controller;

import az.ingress.employee.dto.EmployeeDto;
import az.ingress.employee.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping("/employee")
    public EmployeeDto saveEmployee(@RequestBody EmployeeDto employeeDto) {
        return employeeService.saveEmployee(employeeDto);
    }

    @PutMapping("/employee/{employeeId}")
    public EmployeeDto updateEmployee(@PathVariable Integer employeeId, @RequestBody EmployeeDto employeeDto) {
        return employeeService.updateEmployee(employeeId,employeeDto);
    }

    @GetMapping("/employee")
    public List<EmployeeDto> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @GetMapping("/employee/{employeeId}")
    public EmployeeDto getEmployeeById(@PathVariable Integer employeeId) {
        return employeeService.getEmployeeById(employeeId);
    }

    @DeleteMapping("/employee/{employeeId}")
    public void deleteEmployee(@PathVariable Integer employeeId) {
        employeeService.deleteEmployee(employeeId);
    }
}
